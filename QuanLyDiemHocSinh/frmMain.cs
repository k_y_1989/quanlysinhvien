﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QuanLyDiemHocSinh
{
    public partial class frmMain : Form
    {
        public int LoaiDN = frmDangNhap.loai;
        public int MaQL = frmDangNhap.Ma;
        public frmMain()
        {
            InitializeComponent();
        }

        #region 1-Nut thoat cua Form.
        private void thoatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region 2-Ham khoi tao ban dau.
        public void KhoiTao()
        {
            this.tsDangNhap.Enabled = true;
            this.tsDangXuat.Enabled = false;
            this.tsThoat.Enabled = true;
            /*-Lien quan den giao vien.*/
            this.tsThemMoiGiaoVien.Enabled = false;
            this.tsXoaBoGiaoVien.Enabled = false;
            this.tsCapNhatGiaoVien.Enabled = false;
            /*-Lien quan den hoc sinh.*/
            this.tsThemMoiHocSinh.Enabled = false;
            this.tsXoaBoHocSinh.Enabled = false;
            this.tsCapNhatHocSinh.Enabled = false;
            /*-Lien quan den mon hoc.*/
            this.tsThemMoiMonHoc.Enabled = false;
            this.tsXoaBoMonHoc.Enabled = false;
            this.tsCapNhatMonHoc.Enabled = false;
            /*-Lien quan den nghiep vu.*/
            this.tsNhapDiem.Enabled = false;
            this.tsSuaDiem.Enabled = false;
            this.tsThongKe.Enabled = false;
            /*-Lien quan den chuc nang.*/
            //this.btnGiaoVien.Enabled = false;
            this.btnHocSinh.Enabled = false;
            this.btnMonHoc.Enabled = false;
        }
        #endregion

        #region 3-Ham load form Chinh.
        private void frmMain_Load(object sender, EventArgs e)
        {
            
            KhoiTao();
            txt1.Text = LoaiDN.ToString();
            txt2.Text = MaQL.ToString();
        }
        #endregion

        #region 4-Ham click hien man hinh dang nhap.
        private void tsDangNhap_Click(object sender, EventArgs e)
        {
            frmDangNhap mh_DangNhap = new frmDangNhap();
            mh_DangNhap.MdiParent = this;
            mh_DangNhap.Show();
        }
        #endregion

        private void btnGiaoVien_Click(object sender, EventArgs e)
        {
            frmDanhSachGiaoVien mh_DanhSachGiaoVien = new frmDanhSachGiaoVien();
            mh_DanhSachGiaoVien.MdiParent = this;
            mh_DanhSachGiaoVien.Show();
        }
    }
}
