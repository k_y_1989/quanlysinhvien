﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QuanLyDiemHocSinh
{
    public partial class frmDangNhap : Form
    {
        public static int loai;
        public static int Ma;
        public frmDangNhap()
        {
            InitializeComponent();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            string ten = txtNguoiDung.Text;
            string matkhau = txtMatKhau.Text;
            ClassDangNhap DN = new ClassDangNhap();
            int kq = DN.Dem(ten, matkhau);
            if (kq == 1)
            {
                MessageBox.Show("Đăng Nhập Thành Công !");
                frmMain chinh = new frmMain();
                List<DANG_NHAP> TT=new List<DANG_NHAP>();
                TT = DN.ThongTin(ten, matkhau);
                DANG_NHAP dong = TT[0];
                loai =Convert.ToInt16(dong.Loai);
                Ma =Convert.ToInt16(dong.MA_QL);
                MessageBox.Show(loai.ToString()+" va "+Ma.ToString());
                chinh.ShowDialog();
                this.Hide();
            }

            else
                MessageBox.Show("Tên Đăng Nhâp hoặc Mật Khẩu sai !");
        }
    }
}
