﻿namespace QuanLyDiemHocSinh
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnMonHoc = new System.Windows.Forms.Button();
            this.btnHocSinh = new System.Windows.Forms.Button();
            this.btnGiaoVien = new System.Windows.Forms.Button();
            this.tsGiaoVien = new System.Windows.Forms.ToolStripMenuItem();
            this.tsThemMoiGiaoVien = new System.Windows.Forms.ToolStripMenuItem();
            this.tsXoaBoGiaoVien = new System.Windows.Forms.ToolStripMenuItem();
            this.tsCapNhatGiaoVien = new System.Windows.Forms.ToolStripMenuItem();
            this.tsHocSinh = new System.Windows.Forms.ToolStripMenuItem();
            this.tsThemMoiHocSinh = new System.Windows.Forms.ToolStripMenuItem();
            this.tsXoaBoHocSinh = new System.Windows.Forms.ToolStripMenuItem();
            this.tsCapNhatHocSinh = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsHeThong = new System.Windows.Forms.ToolStripMenuItem();
            this.tsDangNhap = new System.Windows.Forms.ToolStripMenuItem();
            this.tsDangXuat = new System.Windows.Forms.ToolStripMenuItem();
            this.tsThoat = new System.Windows.Forms.ToolStripMenuItem();
            this.mônHocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsThemMoiMonHoc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsXoaBoMonHoc = new System.Windows.Forms.ToolStripMenuItem();
            this.tsCapNhatMonHoc = new System.Windows.Forms.ToolStripMenuItem();
            this.nghiêpVuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsNhapDiem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsSuaDiem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsThongKe = new System.Windows.Forms.ToolStripMenuItem();
            this.txt1 = new System.Windows.Forms.TextBox();
            this.txt2 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txt2);
            this.panel1.Controls.Add(this.txt1);
            this.panel1.Controls.Add(this.btnMonHoc);
            this.panel1.Controls.Add(this.btnHocSinh);
            this.panel1.Controls.Add(this.btnGiaoVien);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(81, 386);
            this.panel1.TabIndex = 1;
            // 
            // btnMonHoc
            // 
            this.btnMonHoc.Location = new System.Drawing.Point(3, 71);
            this.btnMonHoc.Name = "btnMonHoc";
            this.btnMonHoc.Size = new System.Drawing.Size(75, 23);
            this.btnMonHoc.TabIndex = 2;
            this.btnMonHoc.Text = "Môn Học";
            this.btnMonHoc.UseVisualStyleBackColor = true;
            // 
            // btnHocSinh
            // 
            this.btnHocSinh.Location = new System.Drawing.Point(3, 42);
            this.btnHocSinh.Name = "btnHocSinh";
            this.btnHocSinh.Size = new System.Drawing.Size(75, 23);
            this.btnHocSinh.TabIndex = 1;
            this.btnHocSinh.Text = "Học Sinh";
            this.btnHocSinh.UseVisualStyleBackColor = true;
            // 
            // btnGiaoVien
            // 
            this.btnGiaoVien.Location = new System.Drawing.Point(3, 13);
            this.btnGiaoVien.Name = "btnGiaoVien";
            this.btnGiaoVien.Size = new System.Drawing.Size(75, 23);
            this.btnGiaoVien.TabIndex = 0;
            this.btnGiaoVien.Text = "Giáo Viên";
            this.btnGiaoVien.UseVisualStyleBackColor = true;
            this.btnGiaoVien.Click += new System.EventHandler(this.btnGiaoVien_Click);
            // 
            // tsGiaoVien
            // 
            this.tsGiaoVien.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsThemMoiGiaoVien,
            this.tsXoaBoGiaoVien,
            this.tsCapNhatGiaoVien});
            this.tsGiaoVien.Name = "tsGiaoVien";
            this.tsGiaoVien.Size = new System.Drawing.Size(69, 20);
            this.tsGiaoVien.Text = "Giáo Viên";
            // 
            // tsThemMoiGiaoVien
            // 
            this.tsThemMoiGiaoVien.Name = "tsThemMoiGiaoVien";
            this.tsThemMoiGiaoVien.Size = new System.Drawing.Size(129, 22);
            this.tsThemMoiGiaoVien.Text = "Thêm Mới";
            // 
            // tsXoaBoGiaoVien
            // 
            this.tsXoaBoGiaoVien.Name = "tsXoaBoGiaoVien";
            this.tsXoaBoGiaoVien.Size = new System.Drawing.Size(129, 22);
            this.tsXoaBoGiaoVien.Text = "Xoá Bỏ";
            // 
            // tsCapNhatGiaoVien
            // 
            this.tsCapNhatGiaoVien.Name = "tsCapNhatGiaoVien";
            this.tsCapNhatGiaoVien.Size = new System.Drawing.Size(129, 22);
            this.tsCapNhatGiaoVien.Text = "Cập Nhật";
            // 
            // tsHocSinh
            // 
            this.tsHocSinh.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsThemMoiHocSinh,
            this.tsXoaBoHocSinh,
            this.tsCapNhatHocSinh});
            this.tsHocSinh.Name = "tsHocSinh";
            this.tsHocSinh.Size = new System.Drawing.Size(67, 20);
            this.tsHocSinh.Text = "Học Sinh";
            // 
            // tsThemMoiHocSinh
            // 
            this.tsThemMoiHocSinh.Name = "tsThemMoiHocSinh";
            this.tsThemMoiHocSinh.Size = new System.Drawing.Size(129, 22);
            this.tsThemMoiHocSinh.Text = "Thêm Mới";
            // 
            // tsXoaBoHocSinh
            // 
            this.tsXoaBoHocSinh.Name = "tsXoaBoHocSinh";
            this.tsXoaBoHocSinh.Size = new System.Drawing.Size(129, 22);
            this.tsXoaBoHocSinh.Text = "Xoá Bỏ";
            // 
            // tsCapNhatHocSinh
            // 
            this.tsCapNhatHocSinh.Name = "tsCapNhatHocSinh";
            this.tsCapNhatHocSinh.Size = new System.Drawing.Size(129, 22);
            this.tsCapNhatHocSinh.Text = "Cập nhật";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsHeThong,
            this.tsGiaoVien,
            this.tsHocSinh,
            this.mônHocToolStripMenuItem,
            this.nghiêpVuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(783, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsHeThong
            // 
            this.tsHeThong.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsDangNhap,
            this.tsDangXuat,
            this.tsThoat});
            this.tsHeThong.Name = "tsHeThong";
            this.tsHeThong.Size = new System.Drawing.Size(72, 20);
            this.tsHeThong.Text = "Hệ Thống";
            // 
            // tsDangNhap
            // 
            this.tsDangNhap.Name = "tsDangNhap";
            this.tsDangNhap.Size = new System.Drawing.Size(134, 22);
            this.tsDangNhap.Text = "Đăng Nhập";
            this.tsDangNhap.Click += new System.EventHandler(this.tsDangNhap_Click);
            // 
            // tsDangXuat
            // 
            this.tsDangXuat.Name = "tsDangXuat";
            this.tsDangXuat.Size = new System.Drawing.Size(134, 22);
            this.tsDangXuat.Text = "Đăng Xuất";
            // 
            // tsThoat
            // 
            this.tsThoat.Name = "tsThoat";
            this.tsThoat.Size = new System.Drawing.Size(134, 22);
            this.tsThoat.Text = "Thoát";
            this.tsThoat.Click += new System.EventHandler(this.thoatToolStripMenuItem_Click);
            // 
            // mônHocToolStripMenuItem
            // 
            this.mônHocToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsThemMoiMonHoc,
            this.tsXoaBoMonHoc,
            this.tsCapNhatMonHoc});
            this.mônHocToolStripMenuItem.Name = "mônHocToolStripMenuItem";
            this.mônHocToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.mônHocToolStripMenuItem.Text = "Môn Học";
            // 
            // tsThemMoiMonHoc
            // 
            this.tsThemMoiMonHoc.Name = "tsThemMoiMonHoc";
            this.tsThemMoiMonHoc.Size = new System.Drawing.Size(129, 22);
            this.tsThemMoiMonHoc.Text = "Thêm Mới";
            // 
            // tsXoaBoMonHoc
            // 
            this.tsXoaBoMonHoc.Name = "tsXoaBoMonHoc";
            this.tsXoaBoMonHoc.Size = new System.Drawing.Size(129, 22);
            this.tsXoaBoMonHoc.Text = "Xoá Bỏ";
            // 
            // tsCapNhatMonHoc
            // 
            this.tsCapNhatMonHoc.Name = "tsCapNhatMonHoc";
            this.tsCapNhatMonHoc.Size = new System.Drawing.Size(129, 22);
            this.tsCapNhatMonHoc.Text = "Cập Nhật";
            // 
            // nghiêpVuToolStripMenuItem
            // 
            this.nghiêpVuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsNhapDiem,
            this.tsSuaDiem,
            this.tsThongKe});
            this.nghiêpVuToolStripMenuItem.Name = "nghiêpVuToolStripMenuItem";
            this.nghiêpVuToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.nghiêpVuToolStripMenuItem.Text = "Nghiệp vụ";
            // 
            // tsNhapDiem
            // 
            this.tsNhapDiem.Name = "tsNhapDiem";
            this.tsNhapDiem.Size = new System.Drawing.Size(134, 22);
            this.tsNhapDiem.Text = "Nhập Điểm";
            // 
            // tsSuaDiem
            // 
            this.tsSuaDiem.Name = "tsSuaDiem";
            this.tsSuaDiem.Size = new System.Drawing.Size(134, 22);
            this.tsSuaDiem.Text = "Sửa Điểm";
            // 
            // tsThongKe
            // 
            this.tsThongKe.Name = "tsThongKe";
            this.tsThongKe.Size = new System.Drawing.Size(134, 22);
            this.tsThongKe.Text = "Thống kê";
            // 
            // txt1
            // 
            this.txt1.Location = new System.Drawing.Point(12, 100);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(52, 20);
            this.txt1.TabIndex = 3;
            // 
            // txt2
            // 
            this.txt2.Location = new System.Drawing.Point(12, 126);
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(52, 20);
            this.txt2.TabIndex = 4;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 410);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.ShowIcon = false;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnMonHoc;
        private System.Windows.Forms.Button btnHocSinh;
        private System.Windows.Forms.Button btnGiaoVien;
        private System.Windows.Forms.ToolStripMenuItem tsGiaoVien;
        private System.Windows.Forms.ToolStripMenuItem tsThemMoiGiaoVien;
        private System.Windows.Forms.ToolStripMenuItem tsXoaBoGiaoVien;
        private System.Windows.Forms.ToolStripMenuItem tsCapNhatGiaoVien;
        private System.Windows.Forms.ToolStripMenuItem tsHocSinh;
        private System.Windows.Forms.ToolStripMenuItem tsThemMoiHocSinh;
        private System.Windows.Forms.ToolStripMenuItem tsXoaBoHocSinh;
        private System.Windows.Forms.ToolStripMenuItem tsCapNhatHocSinh;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsHeThong;
        private System.Windows.Forms.ToolStripMenuItem tsDangNhap;
        private System.Windows.Forms.ToolStripMenuItem tsDangXuat;
        private System.Windows.Forms.ToolStripMenuItem tsThoat;
        private System.Windows.Forms.ToolStripMenuItem mônHocToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsThemMoiMonHoc;
        private System.Windows.Forms.ToolStripMenuItem tsXoaBoMonHoc;
        private System.Windows.Forms.ToolStripMenuItem tsCapNhatMonHoc;
        private System.Windows.Forms.ToolStripMenuItem nghiêpVuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsNhapDiem;
        private System.Windows.Forms.ToolStripMenuItem tsSuaDiem;
        private System.Windows.Forms.ToolStripMenuItem tsThongKe;
        private System.Windows.Forms.TextBox txt2;
        private System.Windows.Forms.TextBox txt1;

    }
}

