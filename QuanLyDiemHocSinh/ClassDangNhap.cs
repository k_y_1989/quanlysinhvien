﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuanLyDiemHocSinh
{
    
    class ClassDangNhap
    {
        CSDLDataContext db = new CSDLDataContext();
       public int Dem(string ten, string matkhau)
        {
            var dem = (from n in db.DANG_NHAPs where ten==n.Ten_DN && matkhau==n.Mat_Khau select n).Count();
            return dem;
        }

       public List<DANG_NHAP> ThongTin(string ten, string matkhau)
       {
           var dem = (from n in db.DANG_NHAPs where ten == n.Ten_DN && matkhau == n.Mat_Khau select n );
           return dem.ToList();
       }
    }
}